module "state_store_labels" {
  source     = "git::https://github.com/cloudposse/terraform-terraform-label.git?ref=master"
  namespace  = var.namespace
  stage      = var.stage
  name       = "state_store"
  delimiter  = "-"
}

resource "google_storage_bucket" "state-store" {
  name     = module.state_store_labels.id
  location = var.location
  versioning {
    enabled = var.versioning
  }
}