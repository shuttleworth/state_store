variable "name" {
  type = string
}

variable "location" {
  type = string
  description = "Region for bucket"
}

variable "versioning" {
  type = string
  default = "true"
}

variable "namespace" {
  type = string
}

variable "stage" {
  type = string
  default = "staging"
}